let asc = true;
var monTab;
function initURL() {
  let matable = document.getElementById("body");
  matable.innerHTML = "";

  var monURL = document.getElementById("selectfile").value;
  //console.log(montest)

  fetch(monURL)
    .then((response) => response.json())
    .then((tab) => {
      monTab = tab;
      createTable(monTab);
    })
    .catch((err) => {
      console.log(err);
    });

  function createTable(monTab) {
    let body = document.getElementById("body");
    let table = document.createElement("table");
    table.className = "table";
    table.id = "table";

    body.appendChild(table);

    let header = document.createElement("tr");
    let theader = document.createElement("thead");
    table.appendChild(theader);
    theader.appendChild(header);

    let keys = Object.keys(monTab[0]);

    for (let key of keys) {
      let th = document.createElement("th");
      th.innerHTML = key;
      header.appendChild(th);
    }

    const len = monTab.length;
    let first = true;
    for (let row of monTab) {
      let tr = document.createElement("tr");
      if (first) {
        tr.id = "rowToClone";
        first = false;
      }

      for (const key of keys) {
        const td = document.createElement("td");
        td.setAttribute("contentEditable", true);
        const content = row[key] ;
        td.appendChild(document.createTextNode(content));
        tr.appendChild(td);
        delete row[key];
      }

      for (const key in row) {
        const th = document.createElement("th");
        th.appendChild(document.createTextNode(key));
        keys.push(key);
        header.appendChild(th);
        const td = document.createElement("td");
        const content = row[key] || "";
        td.appendChild(document.createTextNode(content));
        tr.appendChild(td);
      }
      table.appendChild(tr);
    }

    //TRIER
    const getCellValue = (tr, idx) =>
      tr.children[idx].innerText || tr.children[idx].textContent;

    const comparer = (idx, asc) => (a, b) =>
      ((v1, v2) =>
        v1 !== "" && v2 !== "" && !isNaN(v1) && !isNaN(v2)
          ? v1 - v2
          : v1.toString().localeCompare(v2))(
        getCellValue(asc ? a : b, idx),
        getCellValue(asc ? b : a, idx)
      );

    document.querySelectorAll("th").forEach((th) =>
      th.addEventListener("click", () => {
        const table = th.closest("table");
        Array.from(table.querySelectorAll("tr:nth-child(n+2)"))
          .sort(
            comparer(
              Array.from(th.parentNode.children).indexOf(th),
              (asc = !asc)
            )
          )
          .forEach((tr) => table.appendChild(tr));
      })
    );
    async function sauvegarder(e) {
      let save = await fetch("../test/up.php", {
        method: "POST",
        body: JSON.stringify(monTab),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((reponse) => reponse.json())
        .catch((err) => {
          console.log(err);
        });
      console.log(save);
    }
  }
  return monTab;
}

function MonFiltre() {
  let input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value;
  table = document.getElementById("table");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td");
    for (j = 0; j < td.length; j++) {
      if (td[j]) {
        txtValue = td[j].textContent || td[j].innerText;
        if (txtValue.toLowerCase().indexOf(filter) > -1) {
          tr[i].style.display = "table-row";
          break;
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }
}

//AJOUTER UNE LIGNE
function cloneRow(e) {
  var row = document.getElementById("rowToClone");
  var clone = row.cloneNode([true]);
  clone.childNodes.forEach((e) => (e.innerText = ""));
  table.appendChild(clone);
}

//SAUVEGARDE( EN COURS, NE FONCTIONNE PAS)

async function sauvegarder(e) {
   fetch("../test/up.php", {
    method: "POST",
    body: JSON.stringify(monTab),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((reponse) => reponse.json())
    .catch((err) => {
      console.log(err);
    });
  
}

document.getElementById("savedata").addEventListener("click", sauvegarder);
